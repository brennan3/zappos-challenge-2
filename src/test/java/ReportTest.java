
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.springframework.web.client.RestTemplate;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import static org.mockito.Mockito.mock;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;



public class ReportTest {


    @Test
    public void constructor(){
        Report report = new Report();
        assertTrue("Num success set to 0.", report.getNumImagesSuccessOnSave().equals(0));
        assertTrue("Num failed set to 0.", report.getNumImagesFailedOnSave().equals(0));
    }

    @Test
    public void updateImageSuccessStatus(){
        Report report = new Report();
        report.updateImageSuccessStatus(true);
        report.updateImageSuccessStatus(true);
        report.updateImageSuccessStatus(false);
        report.updateImageSuccessStatus(false);
        report.updateImageSuccessStatus(false);
        assertTrue("Num success set to 2.", report.getNumImagesSuccessOnSave().equals(2));
        assertTrue("Num failed set to 3.", report.getNumImagesFailedOnSave().equals(3));
    }

    @Test
    public void printsProperly(){
        String result = "";
        result += " Number of sku rows in file: " + "5" + "\n";
        result += " Number of skus with images saved: " + "2" + "\n";
        result += " Number of skus with images that could not be saved: " + "3" + "\n";

        Report report = new Report();
        report.updateImageSuccessStatus(true);
        report.updateImageSuccessStatus(true);
        report.updateImageSuccessStatus(false);
        report.updateImageSuccessStatus(false);
        report.updateImageSuccessStatus(false);

        assertTrue("Correctly printed report.", report.toString().equals(result));

    }

}
