import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

public class RestClient {

    // Class variables
    private Logger logger = LoggerFactory.getLogger(RestClient.class);
    private RestTemplate productRestTemplate = new RestTemplate();
    private String productServiceUrl;
    private String apiKey;
    private String sku;

    // Constructor
    public RestClient(){}

    // Set the rest template for this class
    public void setProductRestTemplate(RestTemplate restTemplate){
        this.productRestTemplate = restTemplate;
    }

    // Get the class rest template
    public RestTemplate getProductRestTemplate(){
        return this.productRestTemplate;
    }

    // Set api key to be used in the rest call
    public void setApiKey(String apiKey){
        this.apiKey = apiKey;
    }

    // get api key
    public String getApiKey(){
        return this.apiKey;
    }

    // get sku
    public String getSku(){
        return this.sku;
    }

    // Set the sku to be used in the rest call
    public void setSku(String sku){
        this.sku = sku;
    }

    // Set the url of the web service
    public void setProductServiceUrl(String productServiceUrl){
        this.productServiceUrl = productServiceUrl;
    }

    // Get the web service url
    public String getProductServiceUrl(){
        return this.productServiceUrl;
    }

    // Make the call to the web service
    public ProductServiceResponse getProductServiceResponse() {
        ProductServiceResponse productServiceResponse = new ProductServiceResponse();
        try{
            productServiceResponse = this.getProductRestTemplate().getForObject(
                this.getProductServiceUrl() + "{sku}?key=" + this.getApiKey(), ProductServiceResponse.class, this.getSku());

        }
        catch(ResourceAccessException ex){
            throw new ResourceAccessException("Unable to hit web service with url: " + this.getProductServiceUrl() + "{sku}?key=" + this.getApiKey());
        }

        return productServiceResponse;
    }
}