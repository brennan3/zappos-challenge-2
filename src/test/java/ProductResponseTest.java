import org.junit.Test;
import static org.junit.Assert.*;

public class ProductResponseTest {

    @Test
    public void getDefaultImageUrl(){
        ProductResponse productResponse = new ProductResponse();
        productResponse.setDefaultImageUrl("image_url");
        assertTrue("Image url brought back as expected.", productResponse.getDefaultImageUrl().equals("image_url"));
    }
}