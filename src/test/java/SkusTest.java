
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.springframework.web.client.RestTemplate;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import static org.mockito.Mockito.mock;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


public class SkusTest {

    Config config;

    @Before
    public void setUp(){

        // -------------------------------------
        // Need to set up our config object
        // -------------------------------------

        // set config
        config = new Config();

        // location of test file
        String pathToTestFile = config.getCurrentFilePath() + File.separator + "src" + File.separator + "test" + File.separator + "TestFiles" + File.separator + "skus.txt";

        // mock up ioUtils object
        IoUtils mockedIoUtils = mock(IoUtils.class);

        // Override method to return exactly what we want
        when(mockedIoUtils.getUserInput("Enter decryption key: ")).thenReturn("jasypt");
        when(mockedIoUtils.getUserInput("Enter a filename: ")).thenReturn(pathToTestFile);

        // update our config object to use our mocked ioUtils
        config.setIoUtils(mockedIoUtils);

        try{
            config.setConfigProperties();
        }
        catch(EncryptionOperationNotPossibleException e){
            System.out.println(e.getMessage());
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
        catch(NoSuchFieldException e){
            System.out.println(e.getMessage());
        }

    }

    @Test
    public void skusWereSet(){
        try{
            Skus skus = new Skus(config);
            ArrayList<String> skusDummy = new ArrayList<String>();
            skusDummy.add("aaa");
            skusDummy.add("bbb");
            skusDummy.add("ccc");
            assertTrue("Populated sku list from dummy file.", skus.getSkus().equals(skusDummy));
        } catch(IOException ex){
            assertTrue("Populated sku list from dummy file.", false);
        }
    }

}
