import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Scanner;

public class IoUtils {

    private Logger logger = LoggerFactory.getLogger(IoUtils.class);
    private Scanner scanner;

    // constructor
    public IoUtils() {
        Scanner sc =  new Scanner(System.in);
        setScanner(sc);
    }

    // set scanner for this class, this is really just needed for testing
    public void setScanner(Scanner sc){
        this.scanner = sc;
    }

    // get the scanner of this class
    public Scanner getScanner(){
        return this.scanner;
    }

    // Prompt user for input and return it
    public String getUserInput(String msg){
        this.logger.trace("Prompting for user input - " + msg);
        String answer = "";
        while(answer.isEmpty()){
            // re prompt for input
            System.out.println(msg);
            answer = this.getScanner().nextLine();
        }
        return answer;
    }

}