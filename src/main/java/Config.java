import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.properties.EncryptableProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

    private Logger logger = LoggerFactory.getLogger(Config.class);
    private String imageFolderDestination;
    private String skuFilePath;
    private String apiKey;
    private String decryptKey;
    private String resourceFile;
    private String webServiceUrl;
    private IoUtils ioUtils;
    private Validators validators;
    private String currentPath;

    public Config() {
        this.setImageFolderDestination(this.getCurrentFilePath() + File.separator + "images");
        this.setResourceFile("config.properties");
    }

    public void setConfigProperties() throws NoSuchFieldException, IOException, EncryptionOperationNotPossibleException {

        this.promptForFile();
        this.promptForDecryptionKey();

        // Attempt to read properties from config file
        try{
            this.readFromResourceFile();
        }
        catch(EncryptionOperationNotPossibleException e){
            throw e;
        }
        catch(IOException e){
            throw e;
        }

        // Make sure that we have values set for our required properties
        try{
            this.getValidators().validateApiKey(this.getApiKey());
            this.getValidators().validateWebServiceUrl(this.getWebServiceUrl());
        }
        catch(NoSuchFieldException e){
            throw e;
        }
    }


    // prompt the user to enter a file
    public void promptForFile(){
        String file = this.getIoUtils().getUserInput("Enter a filename: ");
        Boolean isValidFile = this.getValidators().doesFileExist(file) && this.getValidators().isTextFile(file);

        // continue prompting until a valid file is entered
        while(!isValidFile) {
            file = this.getIoUtils().getUserInput("Enter a filename: ");
            isValidFile = this.getValidators().doesFileExist(file) && this.getValidators().isTextFile(file);
        }

        // store the entered file
        this.setSkuFilePath(file);
    }

    // prompt user to enter a decryption key
    public void promptForDecryptionKey(){
        this.setDecryptKey(this.getIoUtils().getUserInput("Enter decryption key: "));
    }


    public void readFromResourceFile() throws IOException {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(this.getDecryptKey());
        Properties props = new EncryptableProperties(encryptor);
        InputStream input = null;

        try {
            String filename = this.getResourceFile();
            input = Application.class.getClassLoader().getResourceAsStream(filename);
            if(input==null){
                this.logger.error("Sorry, unable to find " + filename);
            }

            //load a properties file from class path, inside static method
            props.load(input);

            if( props.containsKey("apiKeyEncrypted") ){
                this.setApiKey(props.getProperty("apiKeyEncrypted"));
                this.logger.info("apiKey value from config.properties: " + this.getApiKey());
            }
            else {
                this.setApiKey(props.getProperty(""));
                this.logger.warn("Unable to read apiKey property from config.properties file.");
            }

            if( props.containsKey("productServiceUrl") ){
                this.setWebServiceUrl(props.getProperty("productServiceUrl"));
                this.logger.info("productServiceUrl value from config.properties: " + this.getWebServiceUrl());
            }
            else {
                this.setWebServiceUrl(props.getProperty(""));
                this.logger.warn("Unable to read productServiceUrl property from config.properties file.");
            }

        } catch(NullPointerException ex){
            throw new NullPointerException("Unable to read config.properties file.");
        } catch (IOException ex) {
            throw new IOException("Unable to read config.properties file.");
        } catch (EncryptionOperationNotPossibleException ex){
            throw new EncryptionOperationNotPossibleException("Unable to decrypt in config.properties file.  Re-run program with correct decryption key.");
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    // ---------------------------------------------------
    // Getters and Setters
    // ---------------------------------------------------
    public String getImageFolderDestination(){
        return this.imageFolderDestination;
    }

    public void setImageFolderDestination(String imageFolderDestination){
        this.imageFolderDestination = imageFolderDestination;
    }

    public void setSkuFilePath(String skuFilePath){
        this.skuFilePath = skuFilePath;
    }

    public String getSkuFilePath(){
        return this.skuFilePath;
    }

    public String getApiKey(){
        return this.apiKey;
    }

    public void setApiKey(String apiKey){
        this.apiKey = apiKey;
    }

    public void setDecryptKey(String key){
        this.decryptKey = key;
    }

    public String getDecryptKey(){
        return this.decryptKey;
    }

    public String getResourceFile(){
        return this.resourceFile;
    }
    public void setResourceFile(String resourceFile){
        this.resourceFile = resourceFile;
    }

    public void setWebServiceUrl(String webServiceUrl){
        this.webServiceUrl = webServiceUrl;
    }

    public String getWebServiceUrl(){
        return this.webServiceUrl;
    }

    public void setIoUtils(IoUtils ioUtils){
        this.ioUtils = ioUtils;
    }

    public IoUtils getIoUtils(){
        if(this.ioUtils != null){
            return this.ioUtils;
        }
        else {
            IoUtils newIoUtils = new IoUtils();
            this.setIoUtils(newIoUtils);
            return newIoUtils;
        }
    }

    public void setValidators(Validators validators){
        this.validators = validators;
    }

    public Validators getValidators(){
        if(this.validators != null){
            return this.validators;
        }
        else {
            Validators newValidators = new Validators();
            this.setValidators(newValidators);
            return newValidators;
        }
    }

    // Set current path from a string - factored for testing purposes
    public void setCurrentPath(String currentPath){
        this.currentPath = currentPath;
    }

    // Get the current path
    public String getCurrentFilePath(){
        if(this.currentPath != null){
            return this.currentPath;
        }
        else {
            File file = new File("");
            this.setCurrentPath(file.getAbsolutePath());
            return file.getAbsolutePath();
        }
    }





}
