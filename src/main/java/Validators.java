import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;

public class Validators {

    private Logger logger = LoggerFactory.getLogger(Validators.class);

    public Validators(){}

    // Check to see if web service url has a value
    public boolean validateWebServiceUrl(String url) throws NoSuchFieldException {

        if(url.equals("")){
            throw new NoSuchFieldException("ProductResponse service url was missing from config.properites file");
        }

        if(!url.substring(0,7).equals("http://")){
            throw new NoSuchFieldException("Product web service url did not start with http://");
        }

        return true;
    }

    // Check to see if api key had a value
    public boolean validateApiKey(String apiKey) throws NoSuchFieldException {
        if(apiKey.equals("")){
            throw new NoSuchFieldException("Zappos api key was missing from config.properites file");
        }
        return true;
    }



    // Check to see if a file exists
    //   -If no value is passed in to validate,
    //    then return false as this is invalid
    public boolean doesFileExist(){
        return false;
    }

    // Check to see if a file exists
    //   -See if passed in file exists on file system
    public boolean doesFileExist(String filePathString){
        File f = new File(filePathString);
        if(f.exists() && !f.isDirectory()){
            return true;
        }
        else {
            this.logger.info("Invalid file has been entered.");
            return false;
        }
    }

    // Check to see if a given file is a .txt file
    //   - spec on application was that user would process a .txt file
    public boolean isTextFile(String filePathString){
        String ext = FilenameUtils.getExtension(filePathString);
        if(ext.toUpperCase().equals("TXT")){
            return true;
        }
        else {
            this.logger.info("Please enter a .txt file.");
            return false;
        }
    }

}
