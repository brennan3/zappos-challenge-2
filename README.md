DESCRIPTION
-----------

Java program that will accept a filename on the command line. The file should contain a list of skus for various zappos products.  For each product, its image will be downloaded into the /images directory.

PRE REQS
--------

The project was set up as a maven project.  The program can run without Maven, but Maven can be used to regenerate coverage reports, rebuild the jar file, run tests, etc.

For information on Maven, [visit http://maven.apache.org/](visit http://maven.apache.org/)

INSTALLATION
------------

Clone the git project

	git clone https://brennan3@bitbucket.org/brennan3/zappos-challenge-2.git



EXECUTION
---------

1. Navigate to the project root

1. Execute the target/zappos2-Version-2.0-jar-with-dependencies.jar file

    > java -jar target/zappos2-Version-2.0-jar-with-dependencies.jar

1. When prompted for the decryption key, enter:  jasypt

1. When prompted for a file, enter the path to a sku file on your machine.  A dummy file is located at src/test/TestFiles/zappos.txt

1. At this point, you should see any images that were able to be downloaded in the /images directory off the project root


APPLICATION OUTPUTS
-------------------

The application will output to the user the number of items in the sku file, how many skus had an image that could be downloaded, and how many skus did not have an image that could be downloaded.

Cobertura reports: PROJECT_ROOT/target/site/index.html  (see COBERTURA REPORTING section for more information on how to generate these)

The application will download any images that it finds from the provided sku file into PROJECT_ROOT/images.  The directory will be created if it does not already exist.

A log file will be generated: PROJECT_ROOT/log_output.txt - This will contain logging information and other details surrounding the running of the application.

CONFIGURATION OPTIONS
---------------------

There are 2 resource files located in /src/main/resources

    log4j.properties - log4j settings.  The default log level is set to INFO.  If you wish to change the log level, you can do so in this file.

    config.properties - application settings.  The path to the Product API is set here as well as the encryption key.  The encryption key was encrypted using jasypt.  For more information on jasypt, visit http://www.jasypt.org/.


COBERTURA REPORTING
-------------------

The maven project has been set up so that it uses the cobertura plugin.  This plugin can be used to determine the percentage of code accessed by tests.  To generate the cobertura report:

    > mvn site

You should run through the other maven lifecycle steps to ensure dependencies are downloaded prior to running the maven site command.

This will create the directory target/site in the project.  Inside that directory will be HTML files containing information on the project.  Open the index.html file to view the results.  The left menu will contain reports and project information.  Just a note, this will error if running on a mac with < java 1.7 (see http://mojo.10943.n7.nabble.com/Could-not-find-artifact-com-sun-tools-jar-0-td41089.html, and due to this, I have include generate site files in the git project for review in the event they can't be generated.)

1. Project Information: Variety of pages containing information about the project, including such things as members, dependencies, plugins, etc

1. Project Reports:  Here you will find the Cobertura Test Coverage report.  You can drill down in the report to get an idea of how much coverage the current tests provide.  Line Coverage (how many lines of each class were processed while running tests) as well as Branch Coverage (What % of the possible flows [if/else, try/catch, etc] were processed while running tests) are the 2 covereage types.


PROJECT BACKGROUND
------------------

I set the project up as a Maven application.  I chose this because it made for extremely easy dependency management and because it has nice life cycle options.  All necessary libraries are automatically downloaded.

For encryption, I chose to go with jasypt.  It took very little effort to use and worked seamlessly in reading property information whether it was encrypted or not, ie, the same command is used to get an encrypted property and an unencrypted property.

For logging, I used SLF4J coupled with log4j.  SLF4J provides an interface to allow an application to use a number of different loggers at deployment time.  A simple change in the pom.xml file is all that is needed to change loggers.  log4j provided adequate log levels needed for the application.

My goal was to keep the main application as clean as possible.  Given that, I created a variety of utility classes that do all the heavy lifting.


**ImageWriter**

This class is in charge of saving images to disk.  It handles creation of the /images directory if necessary.

**Config**

This class is responsible for obtaining and tracking all settings of the application.  This includes things like web service url, decryption key, api key, etc.

**Report**

This class tracks the number of successful and unsuccessful image downloads were achieved.

**RestClient**

This class is used to handle hitting the web api.

**IoUtils**

This class is used to handle reading input from a user.

**Application**

This is the main class of the application.

**ProductResponse**

Class to handle the response of the product api product json attribute.

**ProductServiceResponse**

Class to handle the response of the product api.

**TestingMethods**

Class with methods that will be used in multiple test files.

**Skus**

Class that is responsible for obtaining skus from the sku file and tracking these values.

**Transaction**

Class that is responsible for taking a single sku and attempting to ace on it - this hits the web service, reads the image url, then attempts to save the image.

**TransactionRunner**

Class that is responsible for runnings transactions on each sku that has been loaded into the application.

