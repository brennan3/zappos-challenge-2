
import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.springframework.web.client.RestTemplate;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import static org.mockito.Mockito.mock;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


public class ImageWriterTest {

    @Test
    public void constructor(){
        ImageWriter imageWriter = new ImageWriter();
        assertTrue("Default image was saved status is false.", imageWriter.getImageWasSaved().equals(false));
        assertTrue("Default source url is blank.", imageWriter.getSourceUrl().equals(""));
        assertTrue("Default target location is blank.", imageWriter.getImageFolderDestination().equals(""));
    }

    @Test
    public void createImageFolderIfNotExists(){
        // 1. Make sure there is no images directory
        ImageWriter imageWriter = new ImageWriter();
        Config config = new Config();
        String dirString = config.getCurrentFilePath() + File.separator + "images";
        imageWriter.setImageFolderDestination(dirString);

        File folder = new File(dirString);
        if(folder.exists()){
            TestingMethods testingMethods = new TestingMethods();
            testingMethods.deleteFolder(folder);
        }

        // 2. Ensure no directory then attempt to create it
        assertTrue("Folder does not yest exist.", !folder.exists());
        try{
            imageWriter.createImageFolderIfNotExists();
            assertTrue("Folder has been created.", folder.exists());
        } catch(IOException ex){
            assertTrue("Folder has been created.", false);
        }
    }

    @Test
    public void saveImageFromUrl(){
        ImageWriter imageWriter = new ImageWriter();
        Config config = new Config();
        String dirString = config.getCurrentFilePath() + File.separator + "images";

        String imageLocation = config.getCurrentFilePath() + File.separator + "images" + File.separator + "12398_1388509335.png";

        imageWriter.setImageFolderDestination(dirString);
        imageWriter.setSourceUrl("http://l4.zassets.com/assets/hotspot/timeslot_images/12398_1388509335.png");


        try{
            imageWriter.createImageFolderIfNotExists();
            imageWriter.saveImageFromUrl();
            File file = new File(imageLocation);
            assertTrue("Web image was saved.", file.exists());
        } catch(IOException ex){
            assertTrue("Web image was saved.", false);
        }


    }

}
