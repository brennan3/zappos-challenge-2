

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import static org.junit.Assert.*;


public class ValidatorsTest {
    @Test
    public void validateWebServiceUrl1(){
        Validators validators = new Validators();

        try{
            Boolean b = validators.validateWebServiceUrl("httppppp://www.zappos.com");
            assertTrue("Returns false when url doesn't start http://", false);
        }
        catch(NoSuchFieldException ex){
            assertTrue("Returns false when url doesn't start http://", true);
        }
    }

    @Test
         public void validateWebServiceUrl2(){
        Validators validators = new Validators();

        try{
            Boolean b = validators.validateWebServiceUrl("");
        }
        catch(NoSuchFieldException ex){
            assertTrue("Returns false when web service url has no value.", true);
        }
    }

    @Test
    public void validateWebServiceUrl3(){
        Validators validators = new Validators();

        try{
            Boolean b = validators.validateWebServiceUrl("http://www.zappos.com");
            assertTrue("Returns true when url starts with http://", true);
        }
        catch(NoSuchFieldException ex){
            assertTrue("Returns true when url starts with http://", true);
        }
    }


    @Test
    public void validateApiKey1(){
        Validators validators = new Validators();

        try{
            Boolean b = validators.validateApiKey("some_url");
            assertTrue("Returns true when api key has a value.", true);
        }
        catch(NoSuchFieldException ex){
            assertTrue("Returns true when api key has a value.", false);
        }
    }

    @Test
    public void validateApiKey2(){
        Validators validators = new Validators();

        try{
            Boolean b = validators.validateApiKey("");
        }
        catch(NoSuchFieldException ex){
            assertTrue("Returns false when api key has no value.", true);
        }
    }


    @Test
    public void doesFileExist1(){
        Validators validators = new Validators();
        assertTrue("File does not exist when nothing is passed in.", !validators.doesFileExist());
    }

    @Test
    public void doesFileExist2(){
        Validators validators = new Validators();
        assertTrue("File does not exist when junk is passed in.", !validators.doesFileExist("junk_file"));
    }

    @Test
    public void doesFileExist3(){
        Validators validators = new Validators();
        Config config = new Config();

        String file = config.getCurrentFilePath() + File.separator + "src" + File.separator + "test" + File.separator + "TestFiles" + File.separator + "skus.txt";
        assertTrue("File does exist valid file is passed in.", validators.doesFileExist(file));
    }

    @Test
    public void isTextFile1(){
        Validators validators = new Validators();
        Config config = new Config();
        String file = config.getCurrentFilePath() + File.separator + "src" + File.separator + "test" + File.separator + "TestFiles" + File.separator + "skus.txt";
        assertTrue("File is valid when its a text file.", validators.isTextFile(file));
    }

    @Test
    public void isTextFile2(){
        Validators validators = new Validators();
        Config config = new Config();
        String file = config.getCurrentFilePath() + File.separator + "README.md";
        assertTrue("File is invalid when its not a text file.", !validators.isTextFile(file));
    }


}
