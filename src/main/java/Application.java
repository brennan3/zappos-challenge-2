import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Application {

    static private Logger logger = LoggerFactory.getLogger(Application.class);
    static private Config config;
    static private TransactionRunner transactionRunner;

    public Application(){

    }

    public static void main(String args[]) throws IOException  {

        System.out.println("===================================");
        System.out.println("=========Zappos Challenge =========");
        System.out.println("===================================");

        logger.info("===================================");
        logger.info("=========Zappos Challenge =========");
        logger.info("===================================");

        // ----------------------------------
        // Set up our config object
        //  - contains things like api key, web service url, etc
        // ----------------------------------


        try{
            getConfig().setConfigProperties();
        }
        catch(EncryptionOperationNotPossibleException e){
            System.out.println(e.getMessage());
            System.exit(0);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            System.exit(0);
        }
        catch(NoSuchFieldException e){
            System.out.println(e.getMessage());
            System.exit(0);
        }

        // ----------------------------------
        // Set up our transaction runner
        //  - this will grab the skus, determine what urls contain images,
        //    and then attempt to save the various images
        // ----------------------------------


        try{
            getTransactionRunner().setConfig(config);
            getTransactionRunner().setUp();
            System.out.println(getTransactionRunner().getReport());
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
            System.exit(0);
        }

        System.out.println("===================================");
        System.out.println("======== Processing Finished ======");
        System.out.println("===================================");

        logger.info("===================================");
        logger.info("======== Processing Finished ======");
        logger.info("===================================");

    }

    // ------------------------------------
    // setters and getters
    // ------------------------------------
    static public Config getConfig(){
        if(config != null){
            return config;
        }
        else{
            Config newConfig = new Config();
            config = newConfig;
            return newConfig;
        }
    }

    static public TransactionRunner getTransactionRunner(){

        if(transactionRunner != null){
            return transactionRunner;
        }
        else{
            TransactionRunner newTransactionRunner = new TransactionRunner();
            transactionRunner = newTransactionRunner;
            return newTransactionRunner;
        }

    }

}
