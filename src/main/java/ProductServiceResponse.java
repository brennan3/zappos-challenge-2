import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ProductServiceResponse {        // Response

    private String statusCode;
    private ProductResponse[] product;

    public ProductServiceResponse(){

    }

    public String getStatusCode() {
        return statusCode;
    }

    public ProductResponse[] getProduct() {
        return product;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public void setProduct(ProductResponse[] productResponses){
        this.product = productResponses;
    }

}