
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ProductResponse {
    private String defaultImageUrl;

    public String getDefaultImageUrl() {
        return this.defaultImageUrl;
    }

    public void setDefaultImageUrl(String defaultImageUrl){
        this.defaultImageUrl = defaultImageUrl;
    }

    public ProductResponse(){

    }
}