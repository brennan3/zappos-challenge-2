import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.IOException;
import static org.mockito.Mockito.mock;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class ApplicationTest {

    Logger logger = LoggerFactory.getLogger(ApplicationTest.class);
    String[] args = new String[1];
    Application app = new Application();




    @Test
    public void main(){
        // Path to our test sku file

        try{

            // location of test file
            String pathToTestFile = app.getConfig().getCurrentFilePath() + File.separator + "src" + File.separator + "test" + File.separator + "TestFiles" + File.separator + "zappos.txt";

            System.out.println("path: " + pathToTestFile);

            // mock up ioUtils object
            IoUtils mockedIoUtils = mock(IoUtils.class);

            // Override method to return exactly what we want
            when(mockedIoUtils.getUserInput("Enter decryption key: ")).thenReturn("jasypt");
            when(mockedIoUtils.getUserInput("Enter a filename: ")).thenReturn(pathToTestFile);

            // update our config object to use our mocked ioUtils
            app.getConfig().setIoUtils(mockedIoUtils);

            app.main(args);

            assertTrue("decryption key set ", app.getConfig().getDecryptKey().equals("jasypt"));
            assertTrue("sku path set ", app.getConfig().getSkuFilePath().equals(pathToTestFile));


            String result = "";
            result += " Number of sku rows in file: " + "7" + "\n";
            result += " Number of skus with images saved: " + "3" + "\n";
            result += " Number of skus with images that could not be saved: " + "4" + "\n";

            assertTrue("Correctly printed report.", app.getTransactionRunner().getReport().toString().equals(result));

        }
        catch(IOException ex){
            logger.warn(ex.getMessage());
        }
    }



    /*
    @Test
    public void mainWithError(){
        // Path to our test sku file

        try{

            // location of test file
            String pathToTestFile = app.getConfig().getCurrentFilePath() + File.separator + "src" + File.separator + "test" + File.separator + "TestFiles" + File.separator + "zappos.txt";

            System.out.println("path: " + pathToTestFile);

            // mock up ioUtils object
            IoUtils mockedIoUtils = mock(IoUtils.class);

            // Override method to return exactly what we want
            when(mockedIoUtils.getUserInput("Enter decryption key: ")).thenReturn("jasyptx");
            when(mockedIoUtils.getUserInput("Enter a filename: ")).thenReturn(pathToTestFile);

            // update our config object to use our mocked ioUtils
            app.getConfig().setIoUtils(mockedIoUtils);

            app.main(args);

            assertTrue("App blows up with bad decryption key", true);

        }
        catch(IOException ex){
            assertTrue("App blows up with bad decryption key", true);
        }
    }
    */


    @After
    public void tearDown() {
        // After we ran our test, remove any images that might have been saved
        String pathToImageFolder= app.getConfig().getCurrentFilePath() + File.separator + "images";
        File folder = new File(pathToImageFolder);

        TestingMethods testingMethods = new TestingMethods();
        testingMethods.deleteFolder(folder);
    }
}