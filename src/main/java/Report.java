
public class Report {

    private Integer numImagesSuccessOnSave;
    private Integer numImagesFailedOnSave;

    public Report(){
        numImagesSuccessOnSave = new Integer(0);
        numImagesFailedOnSave = new Integer(0);
    }

    public String toString(){
        Integer totalItems = this.getNumImagesSuccessOnSave() + this.getNumImagesFailedOnSave();
        String result = "";
        result += " Number of sku rows in file: " + totalItems + "\n";
        result += " Number of skus with images saved: " + this.getNumImagesSuccessOnSave() + "\n";
        result += " Number of skus with images that could not be saved: " + this.getNumImagesFailedOnSave() + "\n";
        return result;
    }

    public void updateImageSuccessStatus(Boolean wasSaved){
        if(wasSaved){
            this.incrementNumImagesSuccessOnSave();
        }
        else{
            this.inrementNumImagesFailedOnSave();
        }
    }

    // ---------------------------------------------------
    // Getters and Setters
    // ---------------------------------------------------
    public Integer getNumImagesSuccessOnSave(){
        return this.numImagesSuccessOnSave;
    }

    public Integer getNumImagesFailedOnSave(){
        return this.numImagesFailedOnSave;
    }

    public void incrementNumImagesSuccessOnSave(){
        this.numImagesSuccessOnSave++;
    }

    public void inrementNumImagesFailedOnSave(){
        this.numImagesFailedOnSave++;
    }

}
