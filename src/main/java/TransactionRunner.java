import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

public class TransactionRunner {

    private Logger logger = LoggerFactory.getLogger(Config.class);
    private Skus skus;
    private Config config;
    private RestClient restClient;
    private Report report;

    public TransactionRunner(){}

    public void setUp() throws IOException {

        try{
            this.skus = new Skus(this.getConfig());
        }
        catch(IOException ex){
            throw new IOException(ex.getMessage());
        }

        // Run a single transaction for each sku
        for (String sku : this.getSkus().getSkus()) {
            logger.trace("Sku was: " + sku);
            Transaction transaction = new Transaction(this.getConfig());
            transaction.setRestClient(this.getRestClient());
            transaction.setSku(sku);
            try{
                transaction.runTransaction();
            }
            catch(IOException ex){
                throw new IOException(ex.getMessage());
            }

            // Track status of this transaction
            this.getReport().updateImageSuccessStatus(transaction.getWasSuccessful());
        }
    }


    // ---------------------------------------------------
    // Getters and Setters
    // ---------------------------------------------------

    public void setConfig(Config config){
        this.config = config;
    }

    public Config getConfig(){
        return this.config;
    }

    public void setRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    public RestClient getRestClient(){
        if(this.restClient != null){
            return this.restClient;
        }
        else{
            RestClient rc = new RestClient();
            this.setRestClient(rc);
            return rc;
        }
    }

    public Skus getSkus(){
        return this.skus;
    }

    public void setSkus(Skus skus){
        this.skus = skus;
    }

    public Report getReport(){
        if(this.report != null){
            return this.report;
        }
        else{
            Report report = new Report();
            this.setReport(report);
            return report;
        }
    }

    public void setReport(Report report){
        this.report = report;
    }
}
