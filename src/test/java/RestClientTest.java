import org.junit.Test;
import org.mockito.Matchers;
import org.springframework.web.client.RestTemplate;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

public class RestClientTest {


    @Test
    public void getProductServiceResponse(){

        RestClient              rc                              = new RestClient();
        RestTemplate            mockedRestTemplate              = mock(RestTemplate.class);
        ProductServiceResponse  mockedProductServiceResponse    = mock(ProductServiceResponse.class);
        ProductResponse         mockedProduct                   = mock(ProductResponse.class);
        ProductResponse[]       products                        = new ProductResponse[1];
        products[0]                                             = mockedProduct;

        when(mockedProductServiceResponse.getStatusCode()).thenReturn("200");
        when(mockedProductServiceResponse.getProduct()).thenReturn(products);
        when(mockedProduct.getDefaultImageUrl()).thenReturn("this_is_some_image_url");
        when(mockedRestTemplate.getForObject(Matchers.anyString(), Matchers.eq(ProductServiceResponse.class), (String) Matchers.notNull())).thenReturn(mockedProductServiceResponse);

        rc.setApiKey("apikey");
        rc.setSku("sku");
        rc.setProductServiceUrl("http://api.zappos.com/ProductResponse/");
        rc.setProductRestTemplate(mockedRestTemplate);
        ProductServiceResponse productService = rc.getProductServiceResponse();

        assertTrue("Correct status code found.", productService.getStatusCode().equals("200"));
        for (ProductResponse p : productService.getProduct()) {
            assertTrue("Correct image URL found.",p.getDefaultImageUrl().equals("this_is_some_image_url"));
        }

    }

    @Test
    public void setProductServiceUrl(){
        RestClient rc = new RestClient();
        rc.setProductServiceUrl("test_url");
        assertTrue("URL set as expected.", rc.getProductServiceUrl().equals("test_url"));
    }

    @Test
    public void setProductRestTemplate(){
        RestClient rc = new RestClient();
        RestTemplate rt = new RestTemplate();
        rc.setProductRestTemplate(rt);
        assertTrue("Rest Template set as expected.", rc.getProductRestTemplate().equals(rt));
    }

}