import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.junit.Assert.*;

public class ProductServiceResponseTest {

    @Test
    public void setStatusCode(){
        ProductServiceResponse productServiceResponse= new ProductServiceResponse();
        productServiceResponse.setStatusCode("status_code");
        assertTrue("Status code brought back as expected.", productServiceResponse.getStatusCode().equals("status_code"));
    }

    @Test
    public void setProduct(){
        ProductResponse mockedProduct = mock(ProductResponse.class);
        ProductResponse[] products = new ProductResponse[1];
        products[0] = mockedProduct;
        ProductServiceResponse productServiceResponse = new ProductServiceResponse();
        productServiceResponse.setProduct(products);
        assertTrue("Products brought back as expected.", productServiceResponse.getProduct().equals(products));
    }


}