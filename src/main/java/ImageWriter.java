import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

public class ImageWriter {

    private Logger logger = LoggerFactory.getLogger(ImageWriter.class);
    private Boolean imageWasSaved;
    private String sourceUrl;
    private String imageFolderDestination;

    public ImageWriter(){
        imageWasSaved = false;
        sourceUrl = "";
        imageFolderDestination = "";
    }

    // ---------------------------------------------------
    // Utility Methods
    // ---------------------------------------------------
    public void createImageFolderIfNotExists() throws IOException {

        try{
            String dirString = this.getImageFolderDestination();
            File f = new File(dirString);

            if(!f.exists()){
                if(!f.mkdir()){
                    throw new IOException("Unable to create image directory.");
                }
                else {
                    logger.info("Image directory did not exist, creating it now...");
                    logger.info("Directory created: + " + dirString);
                }
            }
            else {
                this.logger.info("Image directory already exists.");
            }
        }
        catch(IOException ex){
            throw new IOException("Unable to create image directory.");
        }
    }

    public void saveImageFromUrl() throws IOException {

        // Try to save the image to the directory
        try{
            URL url = new URL(this.getSourceUrl());
            BufferedImage img = ImageIO.read(url);
            String fileName = this.getSourceUrl().substring(this.getSourceUrl().lastIndexOf('/') + 1, this.getSourceUrl().length());
            File file = new File(this.getImageFolderDestination()+ File.separator + fileName);
            ImageIO.write(img, "jpg", file);
            this.setImageWasSaved(true);
            logger.info("Saved image: " + this.getImageFolderDestination() + File.separator + fileName);
        }
        catch (IOException e) {
            this.setImageWasSaved(false);
            throw new IOException("Unable to save image from url: " + this.getSourceUrl());
        }

    }


    // ---------------------------------------------------
    // Getters and Setters
    // ---------------------------------------------------
    public void setImageWasSaved(Boolean imageWasSaved){
        this.imageWasSaved = imageWasSaved;
    }
    public Boolean getImageWasSaved(){
        return this.imageWasSaved;
    }

    public String getSourceUrl(){
        return this.sourceUrl;
    }
    public void setSourceUrl(String sourceUrl){
        this.sourceUrl = sourceUrl;
    }

    public void setImageFolderDestination(String imageFolderDestination){
        this.imageFolderDestination = imageFolderDestination;
    }
    public String getImageFolderDestination(){
        return this.imageFolderDestination;
    }

}
