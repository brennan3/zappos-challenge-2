import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;

public class TestingMethods {

    private Logger logger = LoggerFactory.getLogger(TestingMethods.class);
    public TestingMethods(){}

    public void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if(files!=null) {
            for(File f: files) {
                if(f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    this.logger.info("Deleting file from image directory...");
                    f.delete();
                }
            }
        }

        this.logger.info("Deleting image directory.");
        folder.delete();
    }
}