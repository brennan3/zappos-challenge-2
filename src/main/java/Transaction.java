import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import java.io.IOException;

public class Transaction {

    private Logger logger = LoggerFactory.getLogger(Transaction.class);

    private ImageWriter imageWriter;
    private Boolean wasSuccessful;
    private String sku;
    private Config config;
    private RestClient restClient;

    public Transaction(Config config){
        this.imageWriter = new ImageWriter();
        this.wasSuccessful = false;
        this.sku = "";
        this.config = config;
    }

    public void runTransaction() throws IOException {

        logger.trace("Running transaction for sku: " + this.getSku());

        // Default success of this transaction to false
        this.setWasSuccessful(false);

        // Set up the location to save the images
        this.imageWriter.setImageFolderDestination(this.getConfig().getImageFolderDestination());

        // Create the images folder if it does not exist
        try{
            this.imageWriter.createImageFolderIfNotExists();
        }
        catch(IOException ex){
            // Pass this up the chain as we do not want to continue
            throw new IOException(ex.getMessage());
        }

        // Determine the image url from the sku
        this.getRestClient().setSku(sku.trim());
        this.getRestClient().setProductServiceUrl(this.getConfig().getWebServiceUrl());
        this.getRestClient().setApiKey(this.getConfig().getApiKey());

        try{
            ProductServiceResponse productServiceResponse = restClient.getProductServiceResponse();
            for (ProductResponse pr : productServiceResponse.getProduct()) {
                this.logger.info("Image url from product: " + pr.getDefaultImageUrl());
                this.imageWriter.setSourceUrl(pr.getDefaultImageUrl());
            }
        } catch(HttpClientErrorException ex){
            // Allow processing to continue...
            this.logger.warn(ex.getMessage());
        } catch (ResourceAccessException ex){
            this.logger.warn(ex.getMessage());
        }


        // Save our image
        try{
            imageWriter.saveImageFromUrl();
            if(imageWriter.getImageWasSaved()){
                this.setWasSuccessful(true);
            }
            else {
                this.setWasSuccessful(false);
            }
        }
        catch(IOException ex){
            // Allow processing to continue...
            logger.info(ex.getMessage());
        }
    }



    // ---------------------------------------------------
    // Getters and Setters
    // ---------------------------------------------------
    public void setImageWriter(ImageWriter imageWriter){
        this.imageWriter = imageWriter;
    }

    public ImageWriter getImageWriter(){
        return this.imageWriter;
    }

    public String getSku(){
        return this.sku;
    }

    public void setSku(String sku){
        this.sku = sku;
    }

    public void setWasSuccessful(Boolean wasSuccessful){
        this.wasSuccessful = wasSuccessful;
    }

    public Boolean getWasSuccessful(){
        return this.wasSuccessful;
    }

    public Config getConfig(){
        return this.config;
    }

    public void setConfig(Config config){
        this.config = config;
    }

    public void setRestClient(RestClient restClient) {
        this.restClient = restClient;
    }

    public RestClient getRestClient(){
        return this.restClient;
    }


}
