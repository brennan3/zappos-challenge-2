import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Skus {

    private Logger logger = LoggerFactory.getLogger(Skus.class);
    private ArrayList<String> skus;
    private Config config;
    private String skuFilePath;


    public Skus(Config config) throws IOException {

        logger.trace("New Skus object created.");

        this.skus = new ArrayList<String>();
        this.setConfig(config);
        this.setSkuFilePath(config.getSkuFilePath());
        try{
            this.loadSkus();
        }
        catch(IOException ex){
            throw new IOException("Unable to read skus from the sku file.");
        }
    }

    public void addSku(String sku){
        this.getSkus().add(sku);
    }

    public void loadSkus() throws IOException {

        File f = new File(this.getSkuFilePath());

        try {
            BufferedReader in = new BufferedReader(new FileReader(f.getAbsoluteFile()));
            String line = in.readLine();
            while(line != null) {
                this.addSku(line);
                line = in.readLine();
            }
            in.close();
        }  catch (IOException e) {
            throw new IOException("Exception encountered. IOExeption while reading sku file.");
        }

    }


    // ---------------------------------------------------
    // Getters and Setters
    // ---------------------------------------------------
    public ArrayList<String> getSkus(){
        return this.skus;
    }

    public String getSkuFilePath(){
        return this.skuFilePath;
    }

    public void setSkuFilePath(String skuFilePath){
        this.skuFilePath = skuFilePath;
    }

    public void setConfig(Config config){
        this.config = config;
    }

    public Config getConfig(){
        return this.config;
    }

}
