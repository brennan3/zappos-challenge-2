

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.springframework.web.client.RestTemplate;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import static org.mockito.Mockito.mock;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


public class TransactionRunnerTest {

    Config config;

    @Before
    public void setUp(){

        // -------------------------------------
        // Need to set up our config object
        // -------------------------------------

        // set config
        config = new Config();

        // location of test file
        String pathToTestFile = config.getCurrentFilePath() + File.separator + "src" + File.separator + "test" + File.separator + "TestFiles" + File.separator + "zappos.txt";

        // mock up ioUtils object
        IoUtils mockedIoUtils = mock(IoUtils.class);

        // Override method to return exactly what we want
        when(mockedIoUtils.getUserInput("Enter decryption key: ")).thenReturn("jasypt");
        when(mockedIoUtils.getUserInput("Enter a filename: ")).thenReturn(pathToTestFile);

        // update our config object to use our mocked ioUtils
        config.setIoUtils(mockedIoUtils);

        try{
            config.setConfigProperties();
        }
        catch(EncryptionOperationNotPossibleException e){
            System.out.println(e.getMessage());
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
        catch(NoSuchFieldException e){
            System.out.println(e.getMessage());
        }

    }

    @Test
    public void bombOnNoSkuFile(){
        try{
            config.setSkuFilePath("junk_file");
            TransactionRunner transactionRunner = new TransactionRunner();
            transactionRunner.setConfig(config);
            transactionRunner.setUp();
            assertTrue("Bombed when sku file was junk.", false);
        } catch(IOException ex){
            assertTrue("Bombed when sku file was junk.", true);
        }
    }

    @Test
    public void worksWithValidSkuFile(){
        try{
            TransactionRunner transactionRunner = new TransactionRunner();
            transactionRunner.setConfig(config);
            transactionRunner.setUp();
            assertTrue("Success with valid sku file.", true);
            assertTrue("Found 3 images with valid sku file.", transactionRunner.getReport().getNumImagesSuccessOnSave().equals(3));
            assertTrue("Found 4 failed images with valid sku file.", transactionRunner.getReport().getNumImagesFailedOnSave().equals(4));
        } catch(IOException ex){
            assertTrue("Success with valid sku file", false);
        }
    }


}
