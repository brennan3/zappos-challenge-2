import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.util.Scanner;
import static org.junit.Assert.*;

public class IoUtilsTest {

    @Test
    public void setScanner(){
        IoUtils ioUtils = new IoUtils ();
        Scanner scanner = new Scanner(System.in);
        ioUtils.setScanner(scanner);
        assertTrue("Scanner set as expected.", ioUtils.getScanner().equals(scanner));
    }

    @Test
    public void getUserInput(){
        IoUtils ioUtils = new IoUtils();
        ByteArrayInputStream in = new ByteArrayInputStream("this_was_input_from_user".getBytes());
        Scanner s = new Scanner(in);
        ioUtils.setScanner(s);
        assertTrue("Expected answer returned from prompt.",ioUtils.getUserInput("dummy").equals("this_was_input_from_user"));
        s.close();
    }


}